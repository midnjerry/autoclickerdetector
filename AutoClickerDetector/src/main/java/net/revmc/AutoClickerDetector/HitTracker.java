package net.revmc.AutoClickerDetector;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class HitTracker {
	private Map<UUID, Integer> mBadClickTracker;
	private static final int HIT_THRESHOLD = 5;

	public HitTracker() {
		mBadClickTracker = new HashMap<UUID, Integer>();
	}

	public void MinusOneHit(UUID uuid) {
		Integer hits = mBadClickTracker.get(uuid);
		if (hits == null) {
			hits = 0;
		}
		hits = Math.min(hits - 1, 0);
		mBadClickTracker.put(uuid, hits);
	}

	public void addOneHit(UUID uuid) {
		Integer hits = mBadClickTracker.get(uuid);
		if (hits == null) {
			hits = 0;
		}
		hits++;
		mBadClickTracker.put(uuid, hits);
	}

	public boolean isOverKickThreshold(UUID uuid) {
		Integer hits = mBadClickTracker.get(uuid);
		if (hits != null && hits >= HIT_THRESHOLD) {
			return true;
		}
		return false;
	}

	public void reset(UUID uuid) {
		mBadClickTracker.put(uuid, 0);
	}

	public void removePlayer(UUID uuid) {
		mBadClickTracker.remove(uuid);
	}

	public boolean hasPlayerRecord(UUID uuid) {
		return mBadClickTracker.containsKey(uuid);
	}

}
