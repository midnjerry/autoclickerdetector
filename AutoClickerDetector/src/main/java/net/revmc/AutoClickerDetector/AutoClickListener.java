package net.revmc.AutoClickerDetector;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class AutoClickListener implements Listener {
	public static final String KICK_MSG = "You shouldn't use autoclickers.";
	private Map<UUID, Map<PlayerInteractEventType, Long>> antiClickSpam;
	private HitTracker hitTracker;
	private static int CLICKS_PER_SECOND = 12;
	private static int MILLISECOND_DELAY = 1000 / CLICKS_PER_SECOND;

	public AutoClickListener() {
		this(new HitTracker());
	}

	public AutoClickListener(HitTracker tracker) {
		antiClickSpam = new HashMap<UUID, Map<PlayerInteractEventType, Long>>();
		hitTracker = tracker;
	}

	@EventHandler
	public void onAutoClickDetect(PlayerInteractEvent event) {
		PlayerInteractEventType eventType = determineEventType(event);
		if (isNotAClick(eventType)) {
			return;
		}

		Map<PlayerInteractEventType, Long> clickTimeStampMap = getPlayerClickRecords(event.getPlayer().getUniqueId());
		long now = System.currentTimeMillis();
		UUID playerID = event.getPlayer().getUniqueId();

		if (clickedTooFast(clickTimeStampMap.get(eventType), now)) {
			hitTracker.addOneHit(playerID);
			if (hitTracker.isOverKickThreshold(playerID)) {
				event.getPlayer().kickPlayer(KICK_MSG);
				hitTracker.reset(playerID);
			}
			event.setCancelled(true);
		} else {
			clickTimeStampMap.put(eventType, now + MILLISECOND_DELAY);
			hitTracker.MinusOneHit(event.getPlayer().getUniqueId());
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		hitTracker.removePlayer(event.getPlayer().getUniqueId());
	}

	private boolean clickedTooFast(Long cooldownUntil, long now) {
		return cooldownUntil != null && cooldownUntil > now;
	}

	private PlayerInteractEventType determineEventType(PlayerInteractEvent event) {
		return PlayerInteractEventType.getType(event.getAction(), event.getHand());
	}

	private Map<PlayerInteractEventType, Long> getPlayerClickRecords(UUID playerID) {
		Map<PlayerInteractEventType, Long> dataMap = antiClickSpam.get(playerID);
		if (dataMap == null) {
			dataMap = new HashMap<PlayerInteractEventType, Long>();
			antiClickSpam.put(playerID, dataMap);
		}
		return dataMap;
	}

	private boolean isNotAClick(PlayerInteractEventType eventType) {
		if (eventType == PlayerInteractEventType.PHYSICAL) {
			return true;
		}
		if (eventType == PlayerInteractEventType.UNKNOWN) {
			Bukkit.getLogger().severe("Unknown PlayerInteractEventType value at " + this.getClass().getName());
			return true;
		}
		return false;
	}

}
