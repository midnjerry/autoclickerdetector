package net.revmc.AutoClickerDetector;

import org.bukkit.event.block.Action;
import org.bukkit.inventory.EquipmentSlot;

public enum PlayerInteractEventType {
	RIGHT_CLICK_AIR_MAIN_HAND, RIGHT_CLICK_AIR_OFF_HAND, RIGHT_CLICK_BLOCK_MAIN_HAND, RIGHT_CLICK_BLOCK_OFF_HAND,
	LEFT_CLICK_BLOCK_MAIN_HAND, LEFT_CLICK_AIR_MAIN_HAND, PHYSICAL, UNKNOWN;

	public static PlayerInteractEventType getType(Action action, EquipmentSlot equipmentSlot) {
		PlayerInteractEventType type = UNKNOWN;
		switch (action) {
		case LEFT_CLICK_AIR:
			type = LEFT_CLICK_AIR_MAIN_HAND;
			break;
		case LEFT_CLICK_BLOCK:
			type = LEFT_CLICK_BLOCK_MAIN_HAND;
			break;
		case PHYSICAL:
			break;
		case RIGHT_CLICK_AIR:
			if (equipmentSlot == EquipmentSlot.HAND) {
				type = RIGHT_CLICK_AIR_MAIN_HAND;
			} else if (equipmentSlot == EquipmentSlot.OFF_HAND) {
				type = RIGHT_CLICK_AIR_OFF_HAND;
			}
			break;
		case RIGHT_CLICK_BLOCK:
			if (equipmentSlot == EquipmentSlot.HAND) {
				type = RIGHT_CLICK_BLOCK_MAIN_HAND;
			} else if (equipmentSlot == EquipmentSlot.OFF_HAND) {
				type = RIGHT_CLICK_BLOCK_OFF_HAND;
			}
			break;
		default:
			type = UNKNOWN;
			break;
		}
		return type;
	}
}
