package net.revmc.AutoClickerDetector.Tests;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.UUID;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import net.revmc.AutoClickerDetector.AutoClickListener;
import net.revmc.AutoClickerDetector.HitTracker;

@RunWith(PowerMockRunner.class)
@PrepareForTest(AutoClickListener.class)
public class OnAutoClickDetectTest {
	@Mock
	private Player player;
	private ItemStack itemstack;
	private Block clickedBlock = null;
	private BlockFace clickedBlockFace = null;
	private PlayerInventory inventory;
	private ItemStack mainHandItem = new ItemStack(Material.DIAMOND);
	private ItemStack offHandItem = new ItemStack(Material.SNOW);
	private AutoClickListener mListener;

	@Before
	public void setUp() {
		inventory = mock(PlayerInventory.class);
		when(inventory.getItemInMainHand()).thenReturn(mainHandItem);
		when(inventory.getItemInOffHand()).thenReturn(offHandItem);

		player = mock(Player.class);
		when(player.getUniqueId()).thenReturn(UUID.fromString("F892A907-C3FC-413A-BC04-945FA110EA3A"));
		when(player.getName()).thenReturn("NoobDad");
		when(player.getInventory()).thenReturn(inventory);

		mListener = new AutoClickListener();
	}

	private PlayerInteractEvent setupEvent(Player player, Action action, EquipmentSlot slot) {
		PlayerInteractEvent event;
		event = new PlayerInteractEvent(player, action, itemstack, clickedBlock, clickedBlockFace, slot);
		event.setCancelled(false);
		return event;
	}

	private void executeEvents(AutoClickListener listener, int timeDelayInMS, PlayerInteractEvent... events) {
		for (PlayerInteractEvent event : events) {
			listener.onAutoClickDetect(event);
			try {
				Thread.sleep(timeDelayInMS);
			} catch (InterruptedException e) {
				e.printStackTrace();
				assertEquals(true, false);
			}
		}
	}

	@Test
	public void RejectTwoCapturedRightClicksBlock() {
		EventPair minecraftAction = new EventPair(Action.RIGHT_CLICK_BLOCK, null, null);
		assertEquals(false, minecraftAction.getMainEvent().isCancelled());
		assertEquals(false, minecraftAction.getOffHandEvent().isCancelled());
		minecraftAction.processEvents(mListener);
		assertEquals(false, minecraftAction.getMainEvent().isCancelled());
		assertEquals(false, minecraftAction.getOffHandEvent().isCancelled());
		minecraftAction = new EventPair(Action.RIGHT_CLICK_BLOCK, null, null);
		minecraftAction.processEvents(mListener);
		assertEquals(true, minecraftAction.getMainEvent().isCancelled());
		assertEquals(true, minecraftAction.getOffHandEvent().isCancelled());
	}

	@Test
	public void MainHandAndOffHandDontCancelOut() {
		EventPair minecraftAction = new EventPair(Action.RIGHT_CLICK_BLOCK, mainHandItem, offHandItem);
		assertEquals(false, minecraftAction.getMainEvent().isCancelled());
		assertEquals(false, minecraftAction.getOffHandEvent().isCancelled());
		minecraftAction.processEvents(mListener);
		assertEquals(false, minecraftAction.getMainEvent().isCancelled());
		assertEquals(false, minecraftAction.getOffHandEvent().isCancelled());
	}

	@Test
	public void RejectTwoCapturedLeftClicksAir() {
		EventPair minecraftAction = new EventPair(Action.LEFT_CLICK_AIR, mainHandItem, offHandItem);
		assertEquals(false, minecraftAction.getMainEvent().isCancelled());
		assertEquals(null, minecraftAction.getOffHandEvent());
		minecraftAction.processEvents(mListener);
		assertEquals(false, minecraftAction.getMainEvent().isCancelled());
		assertEquals(null, minecraftAction.getOffHandEvent());
		minecraftAction = new EventPair(Action.LEFT_CLICK_AIR, mainHandItem, offHandItem);
		minecraftAction.processEvents(mListener);
		assertEquals(true, minecraftAction.getMainEvent().isCancelled());
		assertEquals(null, minecraftAction.getOffHandEvent());
	}

	@Test
	public void AllowTwoCapturedLeftAndRightAirClicks() {
		EventPair minecraftAction = new EventPair(Action.LEFT_CLICK_AIR, mainHandItem, offHandItem);
		assertEquals(false, minecraftAction.getMainEvent().isCancelled());
		assertEquals(null, minecraftAction.getOffHandEvent());
		minecraftAction.processEvents(mListener);
		assertEquals(false, minecraftAction.getMainEvent().isCancelled());
		assertEquals(null, minecraftAction.getOffHandEvent());
		minecraftAction = new EventPair(Action.RIGHT_CLICK_AIR, mainHandItem, offHandItem);
		minecraftAction.processEvents(mListener);
		assertEquals(false, minecraftAction.getMainEvent().isCancelled());
		assertEquals(false, minecraftAction.getOffHandEvent().isCancelled());
	}

	@Test
	public void AllowCapturedLeftAndRejectPairRightAirClicks() {
		EventPair minecraftAction = new EventPair(Action.RIGHT_CLICK_AIR, mainHandItem, offHandItem);
		assertEquals(false, minecraftAction.getMainEvent().isCancelled());
		assertEquals(false, minecraftAction.getOffHandEvent().isCancelled());
		minecraftAction.processEvents(mListener);
		assertEquals(false, minecraftAction.getMainEvent().isCancelled());
		assertEquals(false, minecraftAction.getOffHandEvent().isCancelled());
		minecraftAction = new EventPair(Action.LEFT_CLICK_AIR, mainHandItem, offHandItem);
		minecraftAction.processEvents(mListener);
		assertEquals(false, minecraftAction.getMainEvent().isCancelled());
		assertEquals(null, minecraftAction.getOffHandEvent());
		minecraftAction = new EventPair(Action.RIGHT_CLICK_AIR, mainHandItem, offHandItem);
		minecraftAction.processEvents(mListener);
		assertEquals(true, minecraftAction.getMainEvent().isCancelled());
		assertEquals(true, minecraftAction.getOffHandEvent().isCancelled());
	}

	@Test
	public void RejectCapturedPairLeftAndRejectPairRightAirClicks() {
		PlayerInteractEvent event = setupEvent(player, Action.RIGHT_CLICK_AIR, EquipmentSlot.HAND);
		PlayerInteractEvent event2 = setupEvent(player, Action.LEFT_CLICK_AIR, EquipmentSlot.HAND);
		PlayerInteractEvent event3 = setupEvent(player, Action.RIGHT_CLICK_AIR, EquipmentSlot.HAND);
		PlayerInteractEvent event4 = setupEvent(player, Action.LEFT_CLICK_AIR, EquipmentSlot.HAND);
		executeEvents(mListener, 0, event, event2, event3, event4);
		assertEquals(false, event.isCancelled());
		assertEquals(false, event2.isCancelled());
		assertEquals(true, event3.isCancelled());
		assertEquals(true, event4.isCancelled());
	}

	@Test
	public void AllowDelayedRightAirClicks() {
		PlayerInteractEvent event = setupEvent(player, Action.RIGHT_CLICK_AIR, EquipmentSlot.HAND);
		PlayerInteractEvent event2 = setupEvent(player, Action.LEFT_CLICK_AIR, EquipmentSlot.HAND);
		executeEvents(mListener, 200, event, event2);
		assertEquals(false, event.isCancelled());
		assertEquals(false, event2.isCancelled());
	}

	@Test
	public void SimulateOnAirRightClick_OffHandOnly_TwoEventsLaunched() {
		PlayerInteractEvent event = setupEvent(player, Action.RIGHT_CLICK_AIR, EquipmentSlot.OFF_HAND);
		PlayerInteractEvent event2 = setupEvent(player, Action.RIGHT_CLICK_BLOCK, EquipmentSlot.OFF_HAND);
		executeEvents(mListener, 0, event, event2);
		assertEquals(false, event.isCancelled());
		assertEquals(false, event2.isCancelled());
	}

	@Test
	public void KickAfter10AutoClicks() {
		EventPair minecraftAction = new EventPair(Action.RIGHT_CLICK_BLOCK, null, null);
		minecraftAction.processEvents(mListener);
		assertEquals(false, minecraftAction.getMainEvent().isCancelled());
		assertEquals(false, minecraftAction.getOffHandEvent().isCancelled());

		for (int i = 0; i < 4; i++) {
			minecraftAction = new EventPair(Action.RIGHT_CLICK_BLOCK, null, null);
			minecraftAction.processEvents(mListener);
			assertEquals(true, minecraftAction.getMainEvent().isCancelled());
			assertEquals(true, minecraftAction.getOffHandEvent().isCancelled());
		}

		Mockito.verify(player, Mockito.times(1)).kickPlayer(AutoClickListener.KICK_MSG);
	}

	@Test
	public void OnPlayerLeave() {
		PlayerQuitEvent event = new PlayerQuitEvent(player, null);
		HitTracker hitTracker = new HitTracker();
		hitTracker.addOneHit(player.getUniqueId());
		assertEquals(true, hitTracker.hasPlayerRecord(player.getUniqueId()));
		mListener = new AutoClickListener(hitTracker);
		mListener.onPlayerQuit(event);
		assertEquals(false, hitTracker.hasPlayerRecord(player.getUniqueId()));

	}

	class EventPair {
		private ItemStack mainHand;
		private ItemStack offHand;
		private PlayerInteractEvent mainHandEvent = null;
		private PlayerInteractEvent offHandEvent = null;

		EventPair(Action action, ItemStack mainHand, ItemStack offHand) {
			this.mainHand = mainHand;
			this.offHand = offHand;

			switch (action) {
			case LEFT_CLICK_AIR:
			case LEFT_CLICK_BLOCK:
				mainHandEvent = setupEvent(player, action, EquipmentSlot.HAND);
				break;
			case RIGHT_CLICK_AIR:
				if (mainHand != null) {
					mainHandEvent = setupEvent(player, action, EquipmentSlot.HAND);
				}
				if (offHand != null) {
					offHandEvent = setupEvent(player, action, EquipmentSlot.OFF_HAND);
				}
				break;
			case RIGHT_CLICK_BLOCK:
				mainHandEvent = setupEvent(player, action, EquipmentSlot.HAND);
				offHandEvent = setupEvent(player, action, EquipmentSlot.OFF_HAND);
				break;
			default:
				break;
			}
		}

		public PlayerInteractEvent getMainEvent() {
			return mainHandEvent;
		}

		public PlayerInteractEvent getOffHandEvent() {
			return offHandEvent;
		}

		public void processEvents(AutoClickListener listener) {
			when(inventory.getItemInMainHand()).thenReturn(mainHand);
			when(inventory.getItemInOffHand()).thenReturn(offHand);

			if (mainHandEvent != null) {
				listener.onAutoClickDetect(mainHandEvent);
			}

			if (offHandEvent != null) {
				listener.onAutoClickDetect(offHandEvent);
			}
		}
	}
}
