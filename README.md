# AutoClickerDetector README #

AutoClickerDetector is a plugin that prevents the use of macros and fast script clicking within Minecraft.  This is especially important in Minecraft 1.8 PVP
where a player can attack someone rapid fire without a cooldown rate.

### How it works ###
* Measures clicks per second for each player
* If a certain threshold is reached, a warning is given.
* If the player gets warned again X number of times, they're kicked.
* A cooldown timer is implemented so players aren't kicked for warnings collected over long spans of time.

When a player uses a macro or script, so many clicks are registered that multiple warnings are given in a second.  This flags the software to
kick the player.  Championship-caliber players can also reach incredible speeds but is difficult to maintain over a few seconds.  The software
doesn't kick these players, but players still feel like they've earned a "badge of honor" if they can illicit a warning from the software.

AutoClickerDetector was written using Test Driven Development so most variants of clicks were simulated using Mock objects.

### What is this repository for? ###

This repository is part of my portfolio (Jerry Balderas) of work.  The majority of my Revelations code is private but these samples
should give you an idea of my style.

### How do I get set up? ###

* Import as Maven project to Eclipse IDE
* Deploy project
* Add to /plugin directory of server
* Restart server

### How to run tests ###
* In Eclipse IDE, right click project and run as JUnit tests

### Deployment instructions ###
* In Eclipse IDE, right click project and run as Maven profile.
* For commands: clean install package

### Who do I talk to? ###

* Jerry Balderas


